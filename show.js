  ReactDOM.render(React.createElement(
      "div", {
        className: "cont"
      },
      React.createElement(
        "div", {
          className: "row"
        },
        React.createElement(
          "div", {
            className: "col-md-6"
          },
          React.createElement('div', {
              className: "row"
            },
            React.createElement(
              "div", {
                className: "col-md-4"
              },
              'Name'
            ),
            React.createElement(
              "div", {
                className: "col-md-8"
              },
              'John Doe'
            ),
          ),
          React.createElement('div', {
              className: "row"
            },
            React.createElement(
              "div", {
                className: "col-md-4"
              },
              'Phone'
            ),
            React.createElement(
              "div", {
                className: "col-md-8"
              },
              '123456789'
            ),
          ),
        ),
        React.createElement(
          "div", {
            className: "col-md-6"
          },
          React.createElement('div', {
              className: "row"
            },
            React.createElement(
              "div", {
                className: "col-md-4"
              },
              'Address'
            ),
            React.createElement(
              "div", {
                className: "col-md-8"
              },
              'Lorem ipsum'
            ),
          ),
          React.createElement('div', {
              className: "row"
            },
            React.createElement(
              "div", {
                className: "col-md-4"
              },
              'Note'
            ),
            React.createElement(
              "div", {
                className: "col-md-8"
              },
              'Lorem ipsum'
            ),
          ),
        ),
      ),
      React.createElement(
        "div", {
          className: "row  form-group"
        },        
        React.createElement(
          "a", {
            className: "btn btn-default",
            href: "./edit.html"
           },
           "Edit",
        ),        
        React.createElement(
          "a", {
            className: "btn btn-default",
            href: "./index.html"
           },
           "Delete",
        ),
      ),
    ),
    document.getElementById('mainContainer'));